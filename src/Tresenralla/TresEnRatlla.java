package Tresenralla;

import java.util.Scanner;

public class TresEnRatlla {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int opcions;
		int mesura = 3;
		boolean torn = false;
		char tauler[][] = new char[mesura][mesura];
		char BUIT = '·';
		char jugador1 = 'X';
		char jugador2 = 'O';
		int contador = 0;
		int contador1 = 0;
		int contador2 = 0;

		do {
			opcions = menu();
			switch (opcions) {
			case 1:
				System.out.println(" 					TRES EN RATLLA" + "\r\n"
						+ "L'objectiu és arribar a posar tres peces sobre el tauler (de 3 per 3 posicions) de manera que siguin en línia recta (horitzontal, vertical o diagonal)."
						+ "\r\n");
				break;
			case 2:
				jugarJ(mesura, tauler, BUIT, torn, jugador1, jugador2, contador1, contador2);
				break;
			case 3:
				jugarM(mesura, tauler, BUIT, torn, jugador1, jugador2, contador1, contador2);
				break;
			}

		} while (opcions != 0);

	}

	public static int menu() {
		int variable;
		System.out.println("1. Normes del joc");
		System.out.println("2. Jugar contra un altre jugador");
		System.out.println("3. Jugar contra la màquina");
		System.out.println("0. Sortir");
		variable = sc.nextInt();
		return variable;
	}

	public static void inicialitzarTauler(int mesura, char[][] tauler, char BUIT) {
		for (int i = 0; i < mesura; i++)
			for (int j = 0; j < mesura; j++) {
				tauler[i][j] = BUIT;
			}
	}

	public static void visualitzarTauler(int mesura, char[][] tauler) {
		System.out.print("   ");
		for (int i = 1; i <= mesura; i++) {
			if (i >= 10) {
				System.out.printf("%2d  ", i);
			} else {
				System.out.printf("%1d  ", i);
			}

		}
		System.out.println();
		for (int i = 0; i < mesura; i++) {
			System.out.printf("%2d ", i + 1);
			for (int j = 0; j < mesura; j++) {
				if (j >= 10)
					System.out.print(" ");
				System.out.print(tauler[i][j] + "  ");
			}
			System.out.println("");
		}
	}

	public static boolean fullboard(int mesura, char[][] tauler, char BUIT) {
		int contador = 0;
		for (int i = 0; i < mesura; i++) {
			for (int j = 0; j < mesura; j++) {
				if (tauler[i][j] == BUIT) {
					contador++;
				}
			}
		}
		if (contador == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean guanyador(int mesura, char[][] tauler, char jugador) {
		int contarguanyador = 0;
		for (int i = 0; i < mesura; i++) {
			contarguanyador = 0;
			for (int j = 0; j < mesura; j++) {
				if (tauler[i][j] == jugador) {
					contarguanyador++;
					if (contarguanyador == 3) {
						return true;
					}
				} else {
					contarguanyador = 0;
				}
			}
		}
		if (contarguanyador == 3) {
			return true;
		} else {
			contarguanyador = 0;
		}
		for (int j = 0; j < mesura; j++) {
			contarguanyador = 0;
			for (int i = 0; i < mesura; i++) {
				if (tauler[i][j] == jugador) {
					contarguanyador++;
					if (contarguanyador == 3) {
						return true;
					}
				} else {
					contarguanyador = 0;
				}
			}
		}
		if (contarguanyador == 3) {
			return true;
		} else {
			contarguanyador = 0;
		}
		if (tauler[0][0] == jugador && tauler[1][1] == jugador && tauler[2][2] == jugador) {
			return true;
		}
		if (tauler[0][2] == jugador && tauler[1][1] == jugador && tauler[2][0] == jugador) {
			return true;
		}

		return false;

	}

	public static boolean partida(int mesura, char[][] tauler, char BUIT, boolean torn, char jugador1, char jugador2,
			int contador1, int contador2) {
		boolean flag = false;
		while (fullboard(mesura, tauler, BUIT) == true && !flag) {
			if (!torn) {
				System.out.println("JUGADOR 1, et toca: ");
				System.out.println("Digues la fila: ");
				int fila = sc.nextInt() - 1;
				System.out.println("Digues la columna: ");
				int columna = sc.nextInt() - 1;
				if (tauler[fila][columna] == BUIT) {
					tauler[fila][columna] = jugador1;
					contador1++;
					torn = !torn;
				}
				visualitzarTauler(mesura, tauler);
				flag = guanyador(mesura, tauler, jugador1);
				if (flag) {
					System.out.println("Ha guanyat el Jugador 1 amb " + contador1 + " torns. Bé jugat!!");
				}
			} else {
				System.out.println("JUGADOR 2, et toca: ");
				System.out.println("Digues la fila: ");
				int fila = sc.nextInt() - 1;
				System.out.println("Digues la columna: ");
				int columna = sc.nextInt() - 1;
				if (tauler[fila][columna] == BUIT) {
					tauler[fila][columna] = jugador2;
					contador2++;
					torn = !torn;
				}
				visualitzarTauler(mesura, tauler);
				flag = guanyador(mesura, tauler, jugador2);
				if (flag) {
					System.out.println("Ha guanyat el Jugador 2 amb " + contador2 + " torns. Bé jugat!!");
				}
			}
		}
		return flag;
	}

	public static boolean partidaM(int mesura, char[][] tauler, char BUIT, boolean torn, char jugador1, char jugador2,
			int contador1, int contador2) {
		boolean flag = false;
		while (fullboard(mesura, tauler, BUIT) == true && !flag) {
			if (!torn) {
				System.out.println("JUGADOR 1, et toca: ");
				System.out.println("Digues la fila: ");
				int fila = sc.nextInt() - 1;
				System.out.println("Digues la columna: ");
				int columna = sc.nextInt() - 1;
				if (tauler[fila][columna] == BUIT) {
					tauler[fila][columna] = jugador1;
					contador1++;
					torn = !torn;
				}
				visualitzarTauler(mesura, tauler);
				flag = guanyador(mesura, tauler, jugador1);
				if (flag) {
					System.out.println("Ha guanyat el Jugador 1 amb " + contador1 + " torns. Bé jugat!!");
				}
			} else {
				System.out.println("Li toca a la màquina: ");
				System.out.println("Digues la fila: ");
				int fila = (int) (Math.random() * 3);
				System.out.println(fila + 1);
				System.out.println("Digues la columna: ");
				int columna = (int) (Math.random() * 3);
				System.out.println(columna + 1);
				if (tauler[fila][columna] == BUIT) {
					tauler[fila][columna] = jugador2;
					contador2++;
					torn = !torn;
				} else {
					fila = (int) (Math.random() * 3);
					columna = (int) (Math.random() * 3);
				}
				visualitzarTauler(mesura, tauler);
				flag = guanyador(mesura, tauler, jugador2);
				if (flag) {
					System.out.println("Ha guanyat la màquina amb " + contador2 + " torns. Llàstima!!");
				}
			}
		}
		return flag;
	}

	public static void jugarJ(int mesura, char[][] tauler, char BUIT, boolean torn, char jugador1, char jugador2,
			int contador1, int contador2) {
		inicialitzarTauler(mesura, tauler, BUIT);
		visualitzarTauler(mesura, tauler);
		boolean flag = false;
		flag = partida(mesura, tauler, BUIT, torn, jugador1, jugador2, contador1, contador2)
				&& fullboard(mesura, tauler, BUIT);
		if (!flag && fullboard(mesura, tauler, BUIT) == true) {
			System.out.println("Un empat!! Vinga jugeu un altre cop a veure qui guanya");
		}
		System.out.println(" ");
		System.out.println("Què vols fer? ");

	}

	public static void jugarM(int mesura, char[][] tauler, char BUIT, boolean torn, char jugador1, char jugador2,
			int contador1, int contador2) {
		inicialitzarTauler(mesura, tauler, BUIT);
		visualitzarTauler(mesura, tauler);
		boolean flag = false;
		flag = partidaM(mesura, tauler, BUIT, torn, jugador1, jugador2, contador1, contador2)
				&& fullboard(mesura, tauler, BUIT);
		if (!flag && fullboard(mesura, tauler, BUIT) == true) {
			System.out.println("Un empat!! Vinga jugeu un altre cop a veure qui guanya");
		}
		System.out.println(" ");
		System.out.println("Què vols fer? ");
	}
}
